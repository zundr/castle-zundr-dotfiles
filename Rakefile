require 'rake/clean'

task :default => ['sync']

config_dir = File.dirname(__FILE__)
home_dir = ENV['HOME']

bin_dir = "#{home_dir}/bin"
directory bin_dir

apps_dir = "#{home_dir}/apps"
directory apps_dir

def install_symlinks(source_pattern, target_dir)
  directory target_dir
  FileList[source_pattern].each do |source_file|
    target_link = "#{target_dir}/#{File.basename(source_file)}"
    task :install => target_link
    file target_link => [source_file, target_dir] do
      rm_f target_link, :verbose => true
      ln_s source_file, target_link, :verbose => true
    end
    CLEAN.include target_link
  end
end

# rules for each dotfile
install_symlinks "#{config_dir}/home/.[a-zA-Z0-9_]*", home_dir

# rules for each script in bin
install_symlinks "#{config_dir}/home/bin/*", bin_dir

## default tasks to keep update and install links
desc "updates repo"
task :pull do
  system 'git pull'
end

task :sync => ['pull', 'install']

## Oh my zshell!
omz_dir = "#{home_dir}/.oh-my-zsh"
omz_script = "#{omz_dir}/oh-my-zsh.sh"
task :install => omz_script
file omz_script do
  rm_rf omz_dir
  system "git clone http://github.com/robbyrussell/oh-my-zsh.git #{omz_dir}"  
end

## zshell installer
zsh_url = 'http://sourceforge.net/projects/zsh/files/zsh/5.0.0/zsh-5.0.0.tar.bz2/download'
zsh_tar = 'tmp/zsh-5.0.0.tar.bz2'
zsh_build_dir = 'tmp/zsh-5.0.0'

task :zsh => "#{bin_dir}/zsh"
task :apps => :zsh

file "#{apps_dir}/zsh/bin/zsh" => [apps_dir] do
  ensure_package 'libncurses5-dev'
  system "mkdir -p tmp"
  system "wget #{zsh_url} -O #{zsh_tar}"
  system "tar xf #{zsh_tar} -C tmp"
  system "cd #{zsh_build_dir} && ./configure --prefix=#{apps_dir}/zsh --enable-multibyte --enable-pcre --enable-cap"
  system "cd #{zsh_build_dir} && make install"
end

file "#{bin_dir}/zsh" =>  "#{apps_dir}/zsh/bin/zsh" do
  ln_sf "#{apps_dir}/zsh/bin/zsh", "#{bin_dir}/zsh"
end

## tmux installer
tmux_url = 'http://sourceforge.net/projects/tmux/files/tmux/tmux-1.6/tmux-1.6.tar.gz/download'
tmux_tar = 'tmp/tmux-1.6.tar.gz'
tmux_build_dir = 'tmp/tmux-1.6'

task :tmux => "#{bin_dir}/tmux"
task :apps => :tmux

file "#{apps_dir}/tmux/bin/tmux" => [apps_dir] do
  ensure_package 'libncurses5-dev'
  ensure_package 'libevent-dev'
  system "mkdir -p tmp"
  system "wget #{tmux_url} -O #{tmux_tar}"
  system "tar xf #{tmux_tar} -C tmp"
  system "cd #{tmux_build_dir} && ./configure --prefix=#{apps_dir}/tmux"
  system "cd #{tmux_build_dir} && make install"
end

file "#{bin_dir}/tmux" =>  "#{apps_dir}/tmux/bin/tmux" do
  ln_sf "#{apps_dir}/tmux/bin/tmux", "#{bin_dir}/tmux"
end

## utils
def ensure_package(package)
  system "(dpkg -s #{package} >/dev/null 2>&1) || (echo Installing #{package}; sudo apt-get install #{package})"
end
