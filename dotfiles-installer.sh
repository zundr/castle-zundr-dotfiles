function msg() {
  local color="\033[${1}m"
  local message=$2
  local normal="\033[0m"
  echo -e "${color}${message}${normal}"
}

function link_files() {
  local source_dir=$1
  local pattern=$2
  local target_dir=$3
  echo "** Processing $source_dir"
  local files="$(find $source_dir -mindepth 1 -maxdepth 1 -name "$pattern")"
  if [ -n "$files" ]; then
    mkdir -p $target_dir
    for file in $files; do
      if [ -d $file ]; then
        link_directory $file $target_dir
      else
        link_file $file $target_dir
      fi
    done
  else
    echo "No files to link!"
  fi
}

function link_file() {
  local source_file=$1
  local target_dir=$2
  local file_name="$(basename $source_file)"
  local target_file="$target_dir/$file_name"
  if [ ! -e $target_file ]; then
    cp --symbolic-link $source_file $target_dir
    echo "$file_name: linked!"
  elif [ -L $target_file ]; then
    local old_link="$(readlink $target_file)"
    if [ "$old_link" = "$source_file" ]; then
      echo "$file_name: already linked"
    else
      rm --force $target_file                                                                            
      cp --force --symbolic-link $source_file $target_dir
      echo "$file_name: replaced (was linking to $old_link)"
    fi
  else
    VERSION_CONTROL=numbered cp --backup --symbolic-link $source_file $target_dir
    echo "$file_name: replaced (backed up old file)"
  fi
}

function link_directory() {
  local source_file=$1
  local target_dir=$2
  local file_name="$(basename $source_file)"
  local target_file="$target_dir/$file_name"
  if [ ! -e $target_file ]; then
    ln --symbolic --target-directory=$target_dir $source_file
    echo "$file_name: linked!"
  elif [ -L $target_file ]; then
    local old_link="$(readlink $target_file)"
    if [ "$old_link" = "$source_file" ]; then
      echo "$file_name: already linked"
    else                                                                            
      ln --symbolic --target-directory=$target_dir --force $source_file
      echo "$file_name: replaced (was linking to $old_link)"
    fi
  else
    VERSION_CONTROL=numbered mv $target_file $target_file-old
    ln --symbolic --target-directory=$target_dir $source_file
    echo "$file_name: replaced (backed up old file)"
  fi
}

BOLD="1;31"
GREEN="32"

function ensure_package() {
  local package=$1
  local testfile=$2
  msg $GREEN "Checking for $package ($testfile)..."
  if test -n "$testfile" -a -f "$testfile"; then
    msg $GREEN "Package $1 installed, OK"
    return
  fi
  if dpkg -s $package | grep "install ok installed" >/dev/null 2>&1; then return; fi
  msg $BOLD "Installing $package..."

  if test -z "$NO_SUDO"; then
    msg $GREEN "If you cannot SUDO just press Ctrl-C"
    if sudo apt-get install $package; then
      NO_SUDO=1
    fi
  fi

  # try local install (fails if several versions available :/)
  local filename=$(apt-cache show $package|perl -ne 'print $1 if m/Filename: (.*)/')
  local target_deb=$PWD/tmp/$package.deb
  wget -c http://ftp.de.debian.org/debian/$filename -O $target_deb
  local target_dir=$PWD/tmp/$package
  mkdir -p $target_dir
  dpkg-deb --extract $target_deb $target_dir
  export CFLAGS="-I$target_dir/usr/include $CFLAGS"
  export CPPFLAGS="-I$target_dir/usr/include $CFLAGS"
  export LDFLAGS="-L$target_dir/usr/lib $LDFLAGS"
  export RUBYLIB="$target_dir/usr/lib:$RUBYLIB"
  export PKG_CONFIG_PATH="$target_dir/usr/lib/pkgconfig:$PKG_CONFIG_PATH"

  export PATH="$target_dir/usr/bin:$PATH"
  hash -r
}

function link_binaries() {
  local source_dir=$1
  local bin_dir=$HOME/bin
  mkdir -p $bin_dir
  cp -sfv --target-directory=$bin_dir $source_dir/bin/*
}

function git_clone_or_update() {
  local repo_url=$1
  local target_dir=$2
  if [ -d $target_dir ]; then
    echo "Updating repo..."
    (cd $target_dir && git pull)
  else
    echo "Cloning repo $repo_url..."
    git clone $repo_url $target_dir
  fi
}