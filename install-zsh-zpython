#!/bin/bash -e
apps_dir=$HOME/apps
zsh_dir=$apps_dir/zsh
zsh_zpython_repo=https://bitbucket.org/ZyX_I/zsh.git
zsh_build_dir=tmp/zsh-zpython

# get helper function
wget --no-check-certificate --quiet --timestamping https://raw.github.com/gist/3448412/dotfiles-installer.sh
. dotfiles-installer.sh

ensure_package 'libncurses5-dev'
ensure_package 'libpcre3-dev'

if [ -d "${zsh_build_dir}" ]; then
  (cd ${zsh_build_dir} && git pull)
else
  git clone ${zsh_zpython_repo} ${zsh_build_dir}
fi
cd ${zsh_build_dir} \
  && ./configure --prefix=${zsh_dir} --enable-multibyte --enable-pcre --enable-cap \
  && make -j4 install

# install links in bin dir
link_binaries $zsh_dir

# install oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
  git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
fi
