#!/bin/bash
function git_repo() {
  local module=$1
  local url=$2
  local target=$3
  local install_function=install_$module
  echo "### BEGIN MODULE $1 ###"
  if [ -d $target ]; then
    (cd $target && git pull --rebase && git submodule update --init --recursive)
  else
    git clone --recursive $url $target
  fi
  if [ "$(type -t $install_function)" == "function" ]; then
    $install_function
  fi
  echo "### END MODULE $1 ###"
}

## Prezto ##
git_repo zprezto https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

## FZF ##
function install_fzf() {
  (cd ~/.fzf && ./install)
}
git_repo fzf https://github.com/junegunn/fzf.git ~/.fzf

## Homebrew ##
if [ $(uname) == "Linux" ]; then
  git_repo homebrew https://github.com/Homebrew/linuxbrew.git ~/.linuxbrew
  # Until LinuxBrew is fixed, the following is required.
  # See: https://github.com/Homebrew/linuxbrew/issues/47
  export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig:$PKG_CONFIG_PATH
  ## Setup linux brew
  export LINUXBREWHOME=$HOME/.linuxbrew
  export PATH=$LINUXBREWHOME/bin:$PATH
  export MANPATH=$LINUXBREWHOME/man:$MANPATH
  export PKG_CONFIG_PATH=$LINUXBREWHOME/lib64/pkgconfig:$LINUXBREWHOME/lib/pkgconfig:$PKG_CONFIG_PATH
  export LD_LIBRARY_PATH=$LINUXBREWHOME/lib64:$LINUXBREWHOME/lib:$LD_LIBRARY_PATH
fi

if ! (ctags --version 2>&1 | grep universal >/dev/null); then
  brew uninstall ctags || true
  brew uninstall ctags-fishman || true
  brew tap universal-ctags/universal-ctags
  brew install --HEAD universal-ctags
fi

## tmux plugin manager ##
git_repo tpm https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

## base16 shell ##
git_repo base16 https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell

## homeshick ##
git_repo homeshick https://github.com/andsens/homeshick.git ~/.homesick/repos/homeshick
source ~/.homesick/repos/homeshick/homeshick.sh
homeshick pull castle-zundr-dotfiles || homeshick clone ssh://git@bitbucket.org/zundr/castle-zundr-dotfiles.git

## tmux+vim  ##
brew install tmux --bottle
brew install vim --HEAD --bottle

## pt ##
brew install the_platinum_searcher

## ruby ##
brew install rbenv
brew install ruby-build
eval "$(rbenv init -)"
rbenv install 2.2.2
rbenv global 2.2.2

