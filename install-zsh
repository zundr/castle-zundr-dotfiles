#!/bin/bash -e
apps_dir=$HOME/apps
zsh_dir=$apps_dir/zsh
zsh_version=5.0.0
zsh_url=http://sourceforge.net/projects/zsh/files/zsh/${zsh_version}/zsh-${zsh_version}.tar.bz2/download
zsh_tar=tmp/zsh-${zsh_version}.tar.bz2
zsh_build_dir=tmp/zsh-${zsh_version}
mkdir -p $zsh_build_dir

# get helper function
wget --no-check-certificate --quiet --timestamping https://raw.github.com/gist/3448412/dotfiles-installer.sh
. dotfiles-installer.sh

ensure_package 'libncurses5-dev'
ensure_package 'libpcre3-dev'

wget ${zsh_url} -O - | (cd tmp && tar xj)
cd ${zsh_build_dir} \
  && ./configure --prefix=${zsh_dir} --enable-multibyte --enable-pcre --enable-cap \
  && make -j4 install

# install links in bin dir
link_binaries $zsh_dir

# install oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
  git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
fi
