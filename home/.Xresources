*faceName: Ubuntu\ Mono
*faceSize: 10
!*faceName: Inconsolata\ Medium
!*faceSize: 16
XTerm*termName: xterm-256color

!! for apps that do not support fontconfig
Xft.autohint: 0
Xft.lcdfilter:  lcddefault
Xft.hintstyle:  hintfull
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb

!! URxvt config
URxvt.depth: 32
URxvt.transparent: false
URxvt.fading: 0
URxvt.internalBorder: 3
URxvt.lineSpace: 0

URxvt*loginShell: false
URxvt*termName: xterm-256color
URxvt*intensityStyles: false

! disable printing to avoid sending lots of text to the printer by accident
rxvt.print-pipe: "cat > /dev/null"
urxvt.print-pipe: "cat > /dev/null"

! http://input.fontbureau.com/build/?fontSelection=whole&a=ss&g=ss&i=serifs_round&l=serifs_round&zero=0&asterisk=0&braces=straight&preset=default&line-height=1&accept=I+do&email=
URxvt*font: xft:Input Mono Condensed:style=Medium:size=11
URxvt*boldFont: xft:Input Mono Narrow:style=Black:size=11
URxvt*italicFont: xft:Input Mono Condensed:style=Light:size=11
URxvt*boldItalicFont: xft:Input Mono Narrow:style=Light:size=11
!! fix extra-spacing in fonts
URxvt*letterSpace: -2

!URxvt*imFont:               xft:Ubuntu Mono derivative Powerline:style=Regular:autohint=true
URxvt*imLocale: en_US.UTF-8
URxvt*preeditType: Root

URxvt*perl-ext-common:  default,font-size,clipboard,fullscreen,url-select,timestamp,readline,selection-popup,option-popup,dynamic-colors
URxvt*urlLauncher:      /usr/bin/luakit

URxvt*keysym.F2: perl:dynamic-colors:cycle
URxvt*keysym.F3: perl:dynamic-colors:dark
URxvt*keysym.F4: perl:dynamic-colors:light

URxvt*keysym.S-C-T: perl:tabbed:new_tab

URxvt*matcher.button:   1
URxvt*keysym.C-Delete:  perl:matcher:last
URxvt*keysym.M-Delete:  perl:matcher:list

URxvt*keysym.C-S-Up: perl:font-size:increase
URxvt*keysym.C-S-Down: perl:font-size:decrease

URxvt*keysym.M-u: perl:url-select:select_next

! Disable scrollbar
URxvt*scrollbar: false

! Make the scrollbar look a little less 180s.
URxvt*scrollstyle: plain
URxvt*scrollBar_right: true

! Keep 5K of scrollback and only reset the scroll position on keypresses.
URxvt*saveLines: 5120
URxvt*scrollWithBuffer: true
URxvt*scrollTtyOutput: false
URxvt*scrollTtyKeypress: true

! Set the Urgent hint on bell for console apps without notify-send
URxvt*urgentOnBell: true
! But not in something which will not usually be visible anyway
urxvt_kuake.urgentOnBell: false

! More Konsole/Yakuake-like clipboard keybindings
URxvt*copyCommand:  xclip -i -selection clipboard
URxvt*pasteCommand: xclip -o -selection clipboard
!URxvt*copyCommand:  xsel --input --clipboard
!URxvt*pasteCommand: xsel --output --clipboard
URxvt*keysym.S-C-C: perl:clipboard:copy
URxvt*keysym.S-C-V: perl:clipboard:paste
URxvt*keysym.Mod4-C: perl:clipboard:copy
URxvt*keysym.Mod4-V: perl:clipboard:paste
URxvt*keysym.M-v: perl:clipboard:paste_escaped
URxvt*keysym.F11: perl:fullscreen:switch

! Unbreak zsh keys in URxvt
URxvt*keysym.Home: \033[1~
URxvt*keysym.End: \033[4~

! Match my XTerm additions
URxvt*keysym.C-BackSpace: \033\010
URxvt*keysym.C-Tab: \033r
! TODO: Look into translating button bindings.

! Extend smart selection for the zsh and Python interactive interpreter prompts
URxvt*selection.pattern-0: >>>[ ]+(.+)
URxvt*selection.pattern-1: .+?@.+? .+? %%?[ ]+(.+)

URxvt*selection.pattern-2: (in .+ on line \d+)
URxvt*selection-autotransform.2: s/.* in (.+) on line (\d+)/:e \\Q$1\E\\x0d:$2\\x0d/

! Enable dynamic-colors script
URxvt*dynamicColors: true
xterm*dynamicColors: true

! Stuff to explore:
! http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.7.pod
! http://awesome.naquadah.org/wiki/Urxvt
! https://wiki.archlinux.org/index.php/Rxvt-unicode
!URxvt*mapAlert: true
URxvt*geometry: 160x50
URxvt*pastableTabs: true

! Config for profiles
urxvt_dev4.scrollBar: false
urxvt_tbox.scrollBar: false
!urxvt_dev4.iconFile: /usr/share/icons/Humanity/apps/48/gnome-terminal.svg
!urxvt_tbox.iconFile: /usr/share/icons/Humanity/apps/48/gnome-terminal.svg


! disable Ctrl+Shift turning on iso14755
URxvt*iso14755: False

