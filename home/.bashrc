# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
  # We have color support; assume it's compliant with Ecma-48
  # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
  # a case would tend to support setf rather than setaf.)
  color_prompt=yes
else
  color_prompt=
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# Base 16
base16 apply

# Alias definitions.
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# enable programmable completion features
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# tuenti config
if [ -d /usr/lib/tuenti_tools ]; then
  export TUENTI_TOOLS=/usr/lib/tuenti_tools
  export PATH=$PATH:$TUENTI_TOOLS/bin
  . $TUENTI_TOOLS/env/aliases
  . $TUENTI_TOOLS/env/bash_completion
  . $TUENTI_TOOLS/env/profile
fi

# add rvm if there
if [ -d $HOME/.rvm/bin ]; then
  export PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
fi

# add local libraries
for dir in $HOME/apps/*/lib; do
  LD_LIBRARY_PATH=$dir:$LD_LIBRARY_PATH
done
export LD_LIBRARY_PATH

# load homeshick
if [ -f $HOME/.homesick/repos/homeshick/homeshick.sh ]; then
  source $HOME/.homesick/repos/homeshick/homeshick.sh
  source $HOME/.homesick/repos/homeshick/completions/homeshick-completion.bash
  homeshick refresh --quiet
fi

# add linuxbrew if there
BREW_ROOT=$HOME/.linuxbrew
if [ -d $BREW_ROOT ]; then
  export PATH=$BREW_ROOT/bin:$PATH
  export LD_LIBRARY_PATH=$BREW_ROOT/lib:$LD_LIBRARY_PATH
  export MANPATH=$BREW_ROOT/share/man:$MANPATH
fi
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
