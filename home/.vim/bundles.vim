call plug#begin('~/.vim-bundles')

Plug 'tpope/vim-sensible'
Plug 'mkropat/vim-dwiw2015'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
Plug 'bling/vim-airline'
Plug 'kien/ctrlp.vim'
Plug 'rking/ag.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-sleuth'
Plug 'ap/vim-css-color'

Plug 'valloric/YouCompleteMe', { 'do': './install --clang-completer' }

Plug 'altercation/vim-colors-solarized'
Plug 'chriskempson/base16-vim'

Plug 'fishman/ctags', { 'do': 'make' }

Plug 'Yggdroot/indentLine'

Plug '~/.vim-bundles/eclim'

" Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'

call plug#end()
