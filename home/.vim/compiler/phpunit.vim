if exists("current_compiler")
  " finish
endif
let current_compiler = "phpunit"

if exists(":CompilerSet") != 2    " older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

let s:phpunit_cmd = get(b:, 'phpunit_compiler_cmd', 'phpunit') . ' %:p'
execute 'CompilerSet makeprg=' . escape(s:phpunit_cmd, ' ')

let &l:errorformat = join([
      \ '%E%n) %.%#',
      \ '%Z%m called in %f on line %l%.%#',
      \ '%Z%f:%l',
      \ '%C%m',
      \ '%C',
      \ '%f:%l',
      \ '%-G%.%#',
      \ '%.%#PHP Fatal error: %m in %f on line %l',
      \ '%.%#PHP Warning error: %m in %f on line %l',
      \ '%-G%.%#Running PHP',
      \ '%-G%.%#Sebastian Bergmann%.%#',
      \ '%ITime: %m',
      \ '%ZOK%m',
      \ '%ZFAILURES%m',
      \ '%-G',
      \ ], ',')
