function! z#tags#generate_for_filetype(filetype, dirs)
  let tags_file = '~/.cache/vim/' . a:filetype . '.tags'
  if ! filereadable(tags_file)
    let cmd = 'ctags --languages=' . a:filetype . ' -f ' . tags_file . ' ' . join(a:dirs, ' ')
    call vimproc#system_bg(cmd)
  endif
  execute 'setlocal tags+=' . tags_file
endfunction
