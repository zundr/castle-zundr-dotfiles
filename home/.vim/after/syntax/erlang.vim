" add special case for ignored variables
syn match erlangIgnoredVariable /\<_\w*\>/
hi def link erlangIgnoredVariable erlangComment

