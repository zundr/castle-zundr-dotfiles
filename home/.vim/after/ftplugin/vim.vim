setl expandtab
setl ts=2 sw=2

call z#tags#generate_for_filetype('vim', [$VIMRUNTIME, g:plug_home, '~/.vim/'])
