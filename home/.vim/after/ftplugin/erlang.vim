setl expandtab
setl ts=4 sw=4
" see commentary.txt
setl commentstring=%%\ %s

setl include=^-include

" disable syntastic
let b:syntastic_mode = "passive"

function! ErlangInclude(fname)
  let fname = a:fname
  if !filereadable(fname) && fname[0] != '/'
    let parts = split(fname, '/')
    let app_name = parts[0]
    let path = join(parts[1:], '/')
    let cmd = "erl -noshell -eval 'io:format(\"~s\", [code:lib_dir(" . app_name . ")]).' -s erlang halt"
    let app_path = system(cmd)
    let fname = app_path . '/' . path
  endif
  return fname
endfunction
setl includeexpr=ErlangInclude(v:fname)

call z#tags#generate_for_filetype('erlang', ['/usr/lib/erlang'])

" fix errorformat changed by vimerl, to its default value that supports
" subdirectories
setl efm&vim
" add support for testing errors
let &l:efm = &l:efm . ',%E %n) %m (%f:%l)'
let &l:efm = &l:efm . ',%C%m'
let &l:efm = &l:efm . ',%E**%m'
let &l:efm = &l:efm . ',%C%.%#in function %m (%f\, line %l)'
let &l:efm = &l:efm . ',%C%.%#in call from %m (%f\, line %l)'
let &l:efm = &l:efm . ',%.%#in function %m (%f\, line %l)'
let &l:efm = &l:efm . ',%.%#in call from %m (%f\, line %l)'


" run tests
fun! RunEunitTest()
  let module = substitute(expand('%:t:r'), '_tests', '', '')
  let test_dir = expand('%:p:h') . '/../test'
  let bin_dir = expand('%:p:h') . '/../ebin'
  let deps_dir = $PWD . '/deps/*/ebin'
  let cmd = 'erl +A0 -noinput -boot start_clean -pa ' . test_dir . ' ' . deps_dir . ' -pz ' . bin_dir
        \ . " -eval 'case eunit:test([{module," . module . "}], []) of ok -> ok; error -> halt(1) end.'"
        \ . " -eval 'halt(0).'"
  exe 'Dispatch ' . cmd
endfun

nnoremap <buffer> <space>m :wa<cr>:silent Make eunit-apns<cr>

