function MyTests()
  let errors = len(filter(getqflist(), 'v:val.type'))
  if errors > 0
    call airline#parts#define_accent('tests', 'red')
    return 'FAIL'
  else
    call airline#parts#define_accent('tests', 'green')
    return 'OK'
  endif
endfunction

call airline#parts#define_function('tests', 'MyTests')
call airline#parts#define_accent('tests', 'red')

