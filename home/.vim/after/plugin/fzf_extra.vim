let s:default_height = '25%'

let s:default_action = {
  \ 'ctrl-e': 'edit',
  \ 'ctrl-m': 'sbuffer',
  \ 'ctrl-t': 'tabedit',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

" placeholder for communication between s:cmd/fzf_extra#run and s:cmd_extra_callback
let s:fzf_extra_action = {}

function! fzf_extra#callback(lines) abort
  if empty(a:lines)
    return
  endif
  let key = remove(a:lines, 0)
  let cmd = get(s:fzf_extra_action, key, 'sbuffer')
  for item in a:lines
    try
      silent call s:open_file(item, cmd)
    catch /E94/
      " let num_of_windows = winnr('$')
      " if num_of_windows == 1
      "   if expand('%') == ''
      "     let cmd = 'edit'
      "   elseif winwidth('.') > 2.5 * winheight('.')
      "     let cmd = 'vsplit'
      "   else
      "     let cmd = 'split'
      "   endif
      " else
        let cmd = 'tabedit'
      " endif
      silent call s:open_file(item, cmd)
    endtry
  endfor
endfunction

function! s:open_file(filename, cmd)
  " TODO: move to window if already open, or execute action if not open
  " use it also for buffers
  echom a:cmd . ' ' . s:escape(a:filename)
  execute a:cmd s:escape(a:filename)
endfunction

function! s:escape(path)
  return escape(a:path, ' %#\')
endfunction

" Default launcher
function! fzf_extra#run(opts) abort
  let s:fzf_extra_action = get(g:, 'fzf_action', s:default_action)
  let default_opts = {
        \ 'options': '-x ' . '--expect='.join(keys(s:fzf_extra_action), ','),
        \ 'down': get(g:, 'fzf_height', get(g:, 'fzf_tmux_height', s:default_height)),
        \ }
  if !has_key(a:opts, 'sink') && !has_key(a:opts, 'sink*')
    let default_opts['sink*'] = function('fzf_extra#callback')
  endif
  let opts = extend(default_opts, a:opts)
  call fzf#run(opts)
endfunction

" ----------------------------------------------------------------------------
" Open files
" ----------------------------------------------------------------------------
nnoremap <silent> <Leader><Leader> :call fzf_extra#run({})<cr>
nnoremap <silent> <Leader>v :call fzf_extra#run({'source': 'pt -g "" -f ~/.vim/ ~/.vim-bundles/'})<cr>

" ----------------------------------------------------------------------------
" Choose color scheme
" ----------------------------------------------------------------------------
nnoremap <silent> <Leader>C :call fzf_extra#run({'source': <sid>get_colors(), 'sink': 'color'})<cr>
function! s:get_colors()
return map(split(globpath(&rtp, "colors/*.vim"), "\n"),
      \         "substitute(fnamemodify(v:val, ':t'), '\\..\\{-}$', '', '')"),
endfunction

" ----------------------------------------------------------------------------
" Select buffer
" ----------------------------------------------------------------------------
function! s:buflist()
  redir => ls
  silent ls
  redir END
  return split(ls, '\n')
endfunction

function! fzf_extra#bufopen(e)
  execute 'buffer' matchstr(a:e, '^[ 0-9]*')
endfunction

nnoremap <silent> <Leader><Enter> :call fzf_extra#run({
\   'source':  reverse(<sid>buflist()),
\   'sink':    function('fzf_extra#bufopen'),
\   'down':    len(<sid>buflist()) + 2
\ })<CR>

" ----------------------------------------------------------------------------
" Tmux complete
" ----------------------------------------------------------------------------
function! s:tmux_feedkeys(data)
  echom empty(g:_tmux_q)
  execute 'normal!' (empty(g:_tmux_q) ? 'a' : 'ciW')."\<C-R>=a:data\<CR>"
  startinsert!
endfunction

function! s:tmux_words(query)
  let g:_tmux_q = a:query
  let matches = fzf#run({
  \ 'source':      'tmuxwords.rb --all-but-current --scroll 500 --min 5',
  \ 'sink':        function('s:tmux_feedkeys'),
  \ 'options':     '--no-multi --query='.a:query,
  \ 'tmux_height': '40%'
  \ })
endfunction

inoremap <silent> <C-X><C-T> <C-o>:call <SID>tmux_words(expand('<cWORD>'))<CR>


" ----------------------------------------------------------------------------
" Buffer search
" ----------------------------------------------------------------------------
function! s:line_handler(l)
  let keys = split(a:l, ':\t')
  exec 'buf ' . keys[0]
  exec keys[1]
  normal! ^zz
endfunction

function! s:buffer_lines()
  let res = []
  for b in filter(range(1, bufnr('$')), 'buflisted(v:val)')
    call extend(res, map(getbufline(b,0,"$"), 'b . ":\t" . (v:key + 1) . ":\t" . v:val '))
  endfor
  return res
endfunction

command! FZFLines call fzf_extra#run({
      \   'source':  <sid>buffer_lines(),
      \   'sink':    function('<sid>line_handler'),
      \   'options': '--extended --nth=3..'
      \})
nnoremap <leader>/ :FZFLines<cr>

" ----------------------------------------------------------------------------
" Tag search
" ----------------------------------------------------------------------------
function! TagCommand()
  let cmd = substitute('awk _!/^!/ { print $0 }_ ', '_', "'", 'g') . join(tagfiles(), ' ')
  return cmd
endfunction

function! s:tag_handler(l)
  " let old_tags = &tags
  try
    let tmpfile = tempname()
    echom string(l)
  finally
    " let &tags = old_tags
  endtry
endfunction

command! FZFTag call fzf_extra#run({
      \   'source': 'cat ' . join(tagfiles(), ' '),
      \   'sink': function('<sid>tag_handler'),
      \   'options': '--extended  -d "\\t" --with-nth=1,7,2,4,5,3 --nth=1,2 --tiebreak=begin'
      \   })

" ----------------------------------------------------------------------------
" Locate
" ----------------------------------------------------------------------------
command! -nargs=1 Locate call fzf_extra#run({'source': 'locate <q-args>', 'sink': 'e', 'options': '-m'})

" ----------------------------------------------------------------------------
" Buffers
" ----------------------------------------------------------------------------
function! fzf_extra#buflist()
  redir => ls
  silent ls
  redir END
  return split(ls, '\n')
endfunction

function! fzf_extra#bufopen(e)
  execute 'sbuffer ' matchstr(a:e, '\v[0-9]+')
endfunction

nnoremap <silent> <Leader>b :call fzf_extra#run({
\   'source': reverse(fzf_extra#buflist()),
\   'sink': function('fzf_extra#bufopen'),
\   'down': len(fzf_extra#buflist()) + 2
\ })<CR>


" ----------------------------------------------------------------------------
" Tags in buffer
" ----------------------------------------------------------------------------
function! s:buftags()
  return split(system('ctags -f - ' . expand('%') . ' 2>/dev/null'), '\n')
endfunction

function! s:escape(path)
  return escape(a:path, ' %#\')
endfunction

function! s:tagopen(lines) abort
  if empty(a:lines)
    return
  endif
  let key = remove(a:lines, 0)
  let cmd = get(g:fzf_tag_default_action, key, 'e')
  for item in a:lines
    let parts = split(item, "\t")
    let filename = parts[1]
    let jumpcmd = parts[2]
    if expand('%:p') != fnamemodify(filename, ':p')
      call s:open_file(filename, cmd)
    endif
    execute jumpcmd
  endfor
endfunction

let g:fzf_tag_default_action = {
  \ 'ctrl-m': '',
  \ 'ctrl-t': 'tabedit',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

nnoremap <silent> <Leader>o :call fzf_extra#run({
      \ 'options': '+m --expect=' . join(keys(g:fzf_tag_default_action), ','),
      \ 'source': <sid>buftags(),
      \ 'sink*': function('<sid>tagopen')
      \ })<CR>

nnoremap <silent> <Leader>t :call fzf_extra#run({
      \ 'options': '+m --expect=' . join(keys(g:fzf_tag_default_action), ','),
      \ 'source': 'cat tags',
      \ 'sink*': function('<sid>tagopen')
      \ })<CR>



