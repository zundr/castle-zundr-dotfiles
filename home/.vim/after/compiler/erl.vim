setl makeprg=erl
setl efm&vim
" add support for testing errors
let &l:efm = &l:efm . ',%E**%m'
let &l:efm = &l:efm . ',%Cin function %m (%f\, line %l)'
let &l:efm = &l:efm . ',%Cin call from %m (%f\, line %l)'
