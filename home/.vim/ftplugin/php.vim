function! s:configure_testing_in_buffer()
  let b:phpunit_compiler_cmd='cd tests && ./test_runner --php --stop-on-error --stop-on-failure'
  compiler phpunit
endfunction

if expand('%') =~ 'Test.php$'
  call s:configure_testing_in_buffer()
endif

setl keywordprg=phpdoc

" Allow gf to work with PHP namespaced classes.
function! Php_include_expr(fname)
  let fname = a:fname
  let fname = substitute(fname, '\\\', '/', 'g')
  " XXX hack when using __DIR__ . '/....'
  if fname[0] == '/'
    let fname = expand('%:h') . fname
  endif
  return fname
endfunction
setl includeexpr=Php_include_expr(v:fname)
setl suffixesadd+=.php

