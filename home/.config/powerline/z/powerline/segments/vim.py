# -*- coding: utf-8 -*-

from __future__ import absolute_import
from powerline.theme import requires_segment_info
import vim

@requires_segment_info
def vim_code(segment_info, code):
    try:
        return vim.eval(code)
    except Exception, e:
        return '***'

