-- Requires {{{
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
awful.widget = require("awful.widget")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
-- xrandr
local displays = require("displays")
--Application Menu
require('freedesktop.utils')
require('freedesktop.menu')
-- }}}

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
local home_dir = os.getenv("HOME")
local themes_dir = home_dir .. "/.config/awesome/themes"
local my_theme = 'awesome-solarized/dark'
--local my_theme = 'japanese2'
beautiful.init(themes_dir .. "/" .. my_theme .. "/theme.lua")

-- Themes define colours, icons, and wallpapers
--local my_theme = 'powerarrow-darker'
--beautiful.init(os.getenv("HOME") .. "/.config/awesome/themes/" .. my_theme .. "/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvt"
editor = "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.max,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.magnifier,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.floating,
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
--tags = {}
local l_max = awful.layout.suit.max
local l_tile = awful.layout.suit.fair.horizontal
tags = {
  names  = { "term", "web", "chat", "music", "other1", "other2" },
  layout = { l_max, l_max, l_tile, l_max, l_tile, l_tile }
}
for s = 1, screen.count() do
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
menu_items = freedesktop.menu.new()
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

table.insert(menu_items, { "awesome", myawesomemenu, beautiful.awesome_icon })
mymainmenu = awful.menu.new({ items = menu_items, width = 200 })
mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))


  local vicious = require("vicious")
  local blingbling = require("blingbling")

	local colors_stops =  { {beautiful.green , 0},
                          {beautiful.yellow, 0.5},
                          {beautiful.cyan, 0.70},
                          {beautiful.magenta, 0.79},
                          {beautiful.red, 0.90}
                        }

  --home_fs_usage=blingbling.value_text_box({height = 14, width = 40, v_margin = 3})
	--home_fs_usage:set_text_background_color(beautiful.widget_background)
	--home_fs_usage:set_values_text_color(colors_stops)
	--home_fs_usage:set_font_size(8)
	--home_fs_usage:set_background_color("#00000000")
	--home_fs_usage:set_label("home: $percent %")

	--vicious.register(home_fs_usage, vicious.widgets.fs, "${/home used_p}", 120 )
	
	--root_fs_usage=blingbling.value_text_box({height = 14, width = 40, v_margin = 3})
	--root_fs_usage:set_text_background_color(beautiful.widget_background)
	--root_fs_usage:set_values_text_color(colors_stops)
	----root_fs_usage:set_rounded_size(0.4)
	--root_fs_usage:set_font_size(8)
	--root_fs_usage:set_background_color("#00000000")
	--root_fs_usage:set_label("root: $percent %")

	--vicious.register(root_fs_usage, vicious.widgets.fs, "${/ used_p}", 120 )
	
  
--shutdown=blingbling.system.shutdownmenu() --icons have been set in theme
--reboot=blingbling.system.rebootmenu() --icons have been set in theme
--lock=blingbling.system.lockmenu() --icons have been set in theme
--logout=blingbling.system.logoutmenu()


-- }}}

function setup_screen(s, myfocus)
  -- Create a promptbox for each screen
  mypromptbox[s] = awful.widget.prompt()
  -- Create an imagebox widget which will contains an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  mylayoutbox[s] = awful.widget.layoutbox(s)
  mylayoutbox[s]:buttons(awful.util.table.join(
                         awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                         awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                         awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                         awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
  -- Create a taglist widget
  mytaglist[s]=blingbling.tagslist(s,  awful.widget.taglist.filter.all, mytaglist.buttons)
  --mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

  -- Create the wibox
  mywibox[s] = awful.wibox({ position = "top", screen = s, height = 18 })

  -- Widgets that are aligned to the left
  local left_layout = wibox.layout.fixed.horizontal()
  left_layout:add(mylauncher)
  --left_layout:add(wibox.layout.margin(mytag[s],0,0,2,2))
  left_layout:add(wibox.layout.margin(mytaglist[s],0,0,1,1))
  left_layout:add(mypromptbox[s])
  --left_layout:add(home_fs_usage)
  --left_layout:add(root_fs_usage)
  --left_layout:add(data0_fs_usage)
  --left_layout:add(data1_fs_usage)
  --left_layout:add(games_fs_usage)
  --left_layout:add(mytag[s])

  -- Widgets that are aligned to the right
  local right_layout = wibox.layout.fixed.horizontal()
  add_cpu_graph(right_layout)
  add_mem_graph(right_layout)
  if s == 1 then right_layout:add(wibox.widget.systray()) end
  add_volume_bar(right_layout)
  add_text_clock(right_layout)
  right_layout:add(mylayoutbox[s])
  --right_layout:add(reboot)
  --right_layout:add(shutdown)
  --right_layout:add(logout)
  --right_layout:add(lock)

  local middle_layout = wibox.layout.fixed.horizontal()
  -- Create a tasklist widget
  mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)
  middle_layout:add(mytasklist[s])

  -- Now bring it all together (with the tasklist in the middle)
  local layout = wibox.layout.align.horizontal()
  layout:set_left(left_layout)
  layout:set_middle(middle_layout)
  layout:set_right(right_layout)

  mywibox[s]:set_widget(layout)
end

function add_text_clock(box)
  require("calendar")
  local mytextclock = awful.widget.textclock()
  setup_calendar(mytextclock)
  --calendar(mytextclock)
  box:add(mytextclock)
end

function calendar(mywidget, args)
  local args = args or {}
  mywidget:connect_signal("mouse::enter", function() show_calendar(0) end)
  --mywidget:connect_signal("mouse::leave", function() hide_calendar() end)
end

function add_volume_bar(box)
  local blingbling = require("blingbling")
  --local volume_bar = blingbling.volume({height = 18, width = 40, bar =true, show_text = true, label ="Vol"})
  --if volume_bar then
    --volume_bar:set_filled_color("#BBBBBB")
    --volume_bar:update_master()
    --volume_bar:set_master_control()
    --box:add(volumen_bar)
  --end
end

function add_cpu_graph(box)
  local vicious = require("vicious")
  local blingbling = require("blingbling")
  local cpu_graph = blingbling.line_graph({ height = 18, width = 75, show_text = true, label = "cpu $percent%" })
  vicious.register(cpu_graph, vicious.widgets.cpu, '$1', 2)
  blingbling.popups.htop(cpu_graph, { terminal = terminal })
  box:add(cpu_graph)
end

function add_mem_graph(box)
  local vicious = require("vicious")
  local blingbling = require("blingbling")
  local mem_graph = blingbling.line_graph({ height = 18, width = 75, show_text = true, label = "mem $percent%" })
	vicious.register(mem_graph, vicious.widgets.mem, '$1', 2)
  box:add(mem_graph)
end


--- {{{ Configure screens
local myfocus = {}
for s = 1, screen.count() do
  setup_screen(s, myfocus)
end
--- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey, "Shift" }, "r", function() menubar.show() end),

    -- cycle displays
    awful.key({ modkey, "Shift" }, "p", function() displays.xrandr() end),
    -- lock screen
    awful.key({ "Mod1", "Control" }, "l", function () awful.util.spawn("xdg-screensaver lock") end),
    -- suspend
    awful.key({ "Mod1", "Control" }, "s", function () awful.util.spawn_with_shell("lock-and-suspend") end),

    -- multimedia keys
    awful.key({}, "xf86audiolowervolume", function () volume_set("1db-") end),
    awful.key({ modkey }, "f11", function() volume_set("1db-") end),

    awful.key({}, "xf86audioraisevolume", function () volume_set("1db+") end),
    awful.key({ modkey }, "f12", function() volume_set("1db+") end),

    awful.key({}, "xf86audiomute", function () volume_set("0%") end),
    awful.key({ modkey }, "f10", function() volume_set("0%") end),

    awful.key({}, "xf86homepage", function () awful.util.spawn(browser) end),
    awful.key({}, "xf86mail", function () awful.util.spawn(mailreader) end),
    awful.key({}, "xf86calculator", function () awful.util.spawn("gnome-calculator") end),

    awful.key({}, "xf86audioplay", function() play_pause() end),
    awful.key({ modkey }, "f8", function() play_pause() end),

    awful.key({}, "xf86back", function () play_back() end),
    awful.key({ modkey }, "f7", function() play_back() end),

    awful.key({}, "xf86forward", function () play_forward() end),
    awful.key({ modkey }, "f9", function() play_forward() end),

    awful.key({}, "xf86favorites", function () awful.util.spawn(terminal) end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                           grab_focus()
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                         grab_focus()
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.movetotag(tag)
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.toggletag(tag)
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = true
    --if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
    if titlebars_enabled and (type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        --right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        --right_layout:add(awful.titlebar.widget.stickybutton(c))
        --right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("left")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c, {size =12} ):set_widget(layout)
    end
end)

function save_focus(c)
  myfocus[mouse.screen] = c
end

function grab_focus()
  if myfocus[mouse.screen] then
    local c = myfocus[mouse.screen]
    if c then
      client.focus = c
      c:raise()
    end
  end
end

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("focus", save_focus)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}




---- create a battery widget
--batwidget = awful.widget.progressbar()
--batwidget:set_width(8)
--batwidget:set_height(10)
--batwidget:set_vertical(true)
--batwidget:set_background_color("#494b4f")
--batwidget:set_border_color(nil)
--batwidget:set_color("#aecf96")
----batwidget:set_gradient_colors({ "#aecf96", "#88a175", "#ff5656" })
--local batdir = string.sub(awful.util.pread('cd /sys/class/power_supply && ls -1 -d bat*'), 0, -2)
--vicious.register(batwidget, vicious.widgets.bat, "$2", 61, batdir)

---- cpu widget
--cpuwidget = awful.widget.graph()
--cpuwidget:set_width(50)
--cpuwidget:set_background_color("#494b4f")
--cpuwidget:set_color({ type = "linear", from = { 0, 0 }, to = { 10,0 }, stops = { {0, "#ff5656"}, {0.5, "#88a175"}, 
                    --{1, "#aecf96" }}})
--vicious.register(cpuwidget, vicious.widgets.cpu, "$1")

----memory usage
--memwidget = awful.widget.progressbar()
--memwidget:set_width(8)
--memwidget:set_height(10)
--memwidget:set_vertical(true)
--memwidget:set_background_color("#494b4f")
--memwidget:set_border_color(nil)
--memwidget:set_color({ type = "linear", from = { 0, 0 }, to = { 10,0 }, stops = { {0, "#aecf96"}, {0.5, "#88a175"}, 
                    --{1, "#ff5656"}}})
--vicious.register(memwidget, vicious.widgets.mem, "$1", 13)

---- volume widget
--volicon       = wibox.widget.imagebox()
--volicon:set_image(beautiful.widget_vol)
--volbarwidget  = awful.widget.progressbar()
--volbarwidget:set_width(8)
--volbarwidget:set_vertical(true)
--volbarwidget:set_background_color(beautiful.fg_off_widget)
--volbarwidget:set_border_color(nil)
--volbarwidget:set_color(beautiful.fg_widget)
----volbarwidget:set_gradient_colors({
    ----beautiful.fg_widget,
    ----beautiful.fg_center_widget,
    ----beautiful.fg_end_widget })
----awful.widget.layout.margins[volbarwidget.widget] = { top = 2, bottom = 2 }
---- register widgets
--vicious.register(volbarwidget, vicious.widgets.volume, "$1", 2, "pcm")
---- register buttons
----volbarwidget.buttons(awful.util.table.join(
    ----awful.button({ }, 1, function () awful.util.spawn("gnome-volume-control", false) end),
    ----awful.button({ }, 2, function () awful.util.spawn("amixer -q sset master toggle", false) end),
    ----awful.button({ }, 4, function () awful.util.spawn("amixer -q sset pcm 2db+", false) end),
    ----awful.button({ }, 5, function () awful.util.spawn("amixer -q sset pcm 2db-", false) end)
----))


function play_pause()
  naughty.notify({ text = 'play/pause', timeout = 2 })
  awful.util.spawn("dbus-send --print-reply --dest=org.mpris.mediaplayer2.nuvolaplayer /org/mpris/mediaplayer2 org.mpris.mediaplayer2.player.playpause")
end
function play_back()
  naughty.notify({ text = 'back', timeout = 2 })
  awful.util.spawn("dbus-send --print-reply --dest=org.mpris.mediaplayer2.nuvolaplayer /org/mpris/mediaplayer2 org.mpris.mediaplayer2.player.previous")
end
function play_forward()
  naughty.notify({ text = 'forward', timeout = 2 })
  awful.util.spawn("dbus-send --print-reply --dest=org.mpris.mediaplayer2.nuvolaplayer /org/mpris/mediaplayer2 org.mpris.mediaplayer2.player.next")
end
function volume_set(amount)
  naughty.notify({ text = 'volume: ' .. amount, timeout = 2 })
  awful.util.spawn("amixer -q sset master " .. amount)
end

---- }}}

-- {{{ rules
awful.rules.rules = {
    -- all clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "mplayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    -- set firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "firefox" },
    --   properties = { tag = tags[1][2] } },
    -- all clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = true,
            size_hints_honor = false,
            keys = clientkeys,
            buttons = clientbuttons,
            maximized_vertical = false,
            maximized_horizontal = false
        }
    },
 
    -- handle pidgin windows sensibly.  the buddy list is focused and
    -- set as the master window; conversations are slaves by default.
    {
      rule = { class = "Pidgin" },
      properties = { tag = tags[1][3] }
    },
    { 
        rule = { class = "pidgin", role = "buddy_list" },
        properties = {
        }
    },
    {
        rule = { class = "pidgin", role = "conversation" },
        properties = {
          callback = awful.client.setslave
        }
    },
 
    -- make the 'desktop' window of nautilus sticky, so that it shows in
    -- all tags.  other nautilus windows, like 'file manager' views keep
    -- the normal, default window style.
    {
        rule = { class = "nautilus", instance = "desktop_window" },
        properties = {
            border_width = 0,
            sticky = true
        }
    },
 
    -- disable border for chrome windows.  i usually run chrome windows
    -- fully maximized, so a border doesn't appear useful.
    {
        rule = { class = "google-chrome" },
        properties = {
            border_width = 0,
            tag = tags[1][2]
        }
    },
    {
        rule = { class = "Chromium-browser" },
        properties = {
            border_width = 0,
            tag = tags[1][2]
        }
    },

    {
      rule = { class = "crx_nckgahadagoaajjgafhacjanaoiihapd" },
      properties = { tags = tags[1][3] }
    },
 
    -- some clients are floated by default.
    {
        rule = { class = "gedit" },
        properties = {
            floating = true
        }
    },
}
-- }}}
--
local calendar_popup = nil
local calendar_offset = 25

function hide_calendar()
  if calendar_popup ~= nil then
    naughty.destroy(calendar_popup)
    calendar_popup = nil
    calendar_offset = 25
  end
end

function show_calendar(inc_calendar_offset)
  local save_calendar_offset = calendar_offset
  hide_calendar()
  calendar_offset = save_calendar_offset + inc_calendar_offset
  --calendar_data = displayMonth(3, 2014) -- awful.util.pread('cal')
  calendar_data = awful.util.pread('cal')
  calendar_popup = naughty.notify({ text = calendar_data, timeout = 0, hover_timeout = 1 })
end

function displayMonth(month, year, weekStart)
  local t,wkSt = os.time{year=year, month=month+1, day=0}, weekStart or 1
  local d = os.date("*t", t)
  local mthDays, stDay = d.day,(d.wday-d.day-wkSt+1)%7
  local result = '<table class="month">\n<caption>' .. os.date('%B',t) .. '</caption>\n<tr>'
  for x=0,6 do result = result .. os.date("<th>%a</th>", os.time{year=2006, month=1, day=x+wkSt}) end
  result = result .. '</tr>\n<tr>' .. string.rep('<td></td>', stDay)
  for x=1,mthDays-1 do result = result .. "<td>" .. x .. "</td>" ..  (x+stDay)%7==0 and "</tr>\n<tr>" or "" end
  result = result .. '<td>' .. mthDays .. '</td>' .. string.rep('<td></td>', (7-(mthDays+stDay))%7) .. "</tr>\n</table>\n"
  return result
end

-- {{{
function run_once(cmd, pattern)
  local pattern = pattern or cmd
	awful.util.spawn_with_shell("pgrep -u $USER -x " .. pattern .. " > /dev/null || (" .. cmd .. ")")
end
-- }}}
--

----autostart apps
--run_once("chromium-browser")
--run_once("pidgin")
--run_once("urxvt")
--run_once("tbox", "urxvt_tbox")
--run_once("nm-applet")
---- network manager??
----awful.util.spawn_with_shell("wicd-client --tray")
----awful.util.spawn_with_shell("batti")
----awful.util.spawn_with_shell("dropboxd")
----require("displays")
----awful.util.spawn_with_shell("synclient \
----    vertedgescroll=0 verttwofingerscroll=1 \
----    horizedgescroll=0 horiztwofingerscroll=1 \
----    rtcornerbutton=2 rbcornerbutton=3 \
----    ltcornerbutton=6 lbcornerbutton=7 \
----    fingerlow=10 fingerhigh=20 \
----    vertscrolldelta=-110 horizscrolldelta=-110")
------
------swap keys as in apple keyboard
------ menu alt - altgr win
------ 135 64 - 108 134
----awful.util.spawn_with_shell("xmodmap -e 'keycode 135 = alt_l meta_l alt_l meta_l'")
----awful.util.spawn_with_shell("xmodmap -e 'keycode 64 = super_l nosymbol super_l'")
----awful.util.spawn_with_shell("xmodmap -e 'keycode 108 = super_r nosymbol super_r'")
----awful.util.spawn_with_shell("xmodmap -e 'keycode 134 = iso_level3_shift nosymbol iso_level3_shift'")
----awful.util.spawn_with_shell("parcellite")
----awful.util.spawn_with_shell("xscreensaver -no-splash")
----awful.util.spawn_with_shell("gnome-settings-daemon")
----awful.util.spawn_with_shell("nm-applet")

