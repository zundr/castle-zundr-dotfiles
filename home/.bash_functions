#!/bin/bash

# run different VIM servers for different branches
function mvim() {
  local mvim_exe=$(which mvim)
  local pwd=$(pwd)
  local dirname=$(dirname $pwd)
  local root=$(hg root 2>/dev/null)
  if [ -z "$root" ]; then
    $mvim_exe $*
  else
    local branch=$(hg branch)
    local servername=$(basename $root)
    local servername="${branch} @ ${servername}"
    $mvim_exe --servername "$ervername" $*
  fi
}

function ack ()
{
  if [ -f /usr/bin/ack-grep ]
  then
    ack-grep $*
  elif [ -f /usr/bin/ack ]
  then
    /usr/bin/ack $*
  else
    grep "--exclude=*.svn-base" "--exclude=entries" "--exclude=all-wcprops" "--exclude=.hg" --color -n -R -E $*
  fi
}

