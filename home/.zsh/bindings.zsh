## lower timeout (for Esc mainly)
KEYTIMEOUT=1

## Keybindings (vi!)
bindkey -v
 
# up/down allows to either browse history (if field is empty) or search
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search
bindkey '^K' history-substring-search-up
bindkey '^J' history-substring-search-down

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down

bindkey '^[[1~' beginning-of-line
bindkey '^A' beginning-of-line
bindkey '^[[2~' vi-insert
bindkey '^[[3~' delete-char
bindkey '^[[4~' end-of-line
bindkey '^E' end-of-line
bindkey '^[[5~' history-substring-search-up
bindkey '^[[6~' history-substring-search-down

bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word

bindkey '^W' backward-kill-word    # vi-backward-kill-word
bindkey '^H' backward-delete-char  # vi-backward-delete-char
bindkey '^U' kill-line             # vi-kill-line
bindkey '^?' backward-delete-char  # vi-backward-delete-char

# incremental search backward as in VIM
bindkey -M vicmd '?' history-incremental-search-backward

# increase/decrease number as in vim
_increase_number() {
  local -a match mbegin mend
  [[ $LBUFFER =~ '([0-9]+)[^0-9]*$' ]] &&
    LBUFFER[mbegin,mend]=$(printf %0${#match[1]}d $((10#$match+${NUMERIC:-1})))
}
zle -N increase-number _increase_number
bindkey '^Xa' increase-number
bindkey -s '^Xx' '^[-^Xa'

# The following four widgets require something like the following, to
# load and initialise the `replace-pattern' and `replace-string-again'
# widgets:
#
# zstyle ':zle:replace-pattern' edit-previous false
# autoload -Uz replace-string
# autoload -Uz replace-string-again
# zle -N replace-pattern replace-string
# zle -N replace-string-again
#
# ...and that *before* this file is sourced.

if (( ${+widgets[replace-pattern]} )); then
    bindkey -M vicmd '^x,' ft-replace-pattern
    bindkey -M viins '^x,' ft-replace-pattern
fi

if (( ${+widgets[replace-string-again]} )); then
    bindkey -M vicmd '^x.' replace-string-again
    bindkey -M viins '^x.' replace-string-again
fi

if (( ${+widgets[.history-incremental-pattern-search-backward]} )); then
  bindkey -M viins '^r' history-incremental-pattern-search-backward
  bindkey -M vicmd '^r' history-incremental-pattern-search-backward
else
  bindkey -M viins '^r' history-incremental-search-backward
  bindkey -M vicmd '^r' history-incremental-search-backward
fi
if (( ${+widgets[.history-incremental-pattern-search-forward]} )); then
  bindkey -M viins '^s' history-incremental-pattern-search-forward
  bindkey -M vicmd '^s' history-incremental-pattern-search-forward
else
  bindkey -M viins '^s' history-incremental-search-forward
  bindkey -M vicmd '^s' history-incremental-search-forward
fi

bindkey -M vicmd '^[h' run-help
bindkey -M viins '^[h' run-help
bindkey -M viins '^p'  up-line-or-history
bindkey -M viins '^n'  down-line-or-history
bindkey -M viins '^w'  backward-kill-word
bindkey -M viins '^h'  backward-delete-char
bindkey -M viins '^?'  backward-delete-char

