if [ -n "$TMUX" ]
then
  ## update environment from tmux
  function tmux-update-env() {
    eval $(tmux show-environment | perl -ne 'print "export $1=\"$2\"\n" if m/^([^-]+)=(.+)/;')
  }

  function tmux-update-title() {
    if [ -n "$1" ]; then
      tmux rename-window $1
    fi
  }

  autoload -U add-zsh-hook
  add-zsh-hook precmd tmux-update-env 2>/dev/null || { echo "tmux.zsh: failed to add update-env precmd hook" }
  add-zsh-hook preexec tmux-update-title 2>/dev/null || { echo "tmux.zsh: failed to add update-title preexec hook" }
fi
