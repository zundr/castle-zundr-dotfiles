#
# Executes commands at login pre-zshrc.
#
source "${ZDOTDIR:-$HOME}/.zprezto/runcoms/zprofile"

#
# Editors
#
export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'

