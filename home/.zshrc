ZDOTDIR=$HOME

#
# Add my bin folder
#
if [ -d $HOME/bin ]; then
  export PATH=$HOME/bin:$PATH
fi

# add linuxbrew if there
export LINUXBREWHOME=$HOME/.linuxbrew
if [ -d $LINUXBREWHOME ]; then
  # Until LinuxBrew is fixed, the following is required.
  # See: https://github.com/Homebrew/linuxbrew/issues/47
  export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig:$PKG_CONFIG_PATH
  ## Setup linux brew
  export PATH=$LINUXBREWHOME/bin:$PATH
  export MANPATH=$LINUXBREWHOME/man:$MANPATH
  export PKG_CONFIG_PATH=$LINUXBREWHOME/lib64/pkgconfig:$LINUXBREWHOME/lib/pkgconfig:$PKG_CONFIG_PATH
  export LD_LIBRARY_PATH=$LINUXBREWHOME/lib64:$LINUXBREWHOME/lib:$LD_LIBRARY_PATH
fi

# add my functions
fpath=($HOME/.zsh/functions $fpath)

# setup functions to autoload
autoload -Uz v

# load homeshick completion
if [ -d $HOME/.homesick/repos/homeshick/completions ]; then
  fpath=($HOME/.homesick/repos/homeshick/completions $fpath)
fi

## keyring
if (( $+commands[gnome-keyring-daemon] ));  then
  eval $(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
  export SSH_AUTH_SOCK
fi

#
# Executes commands at the start of an interactive session.
#
source "${ZDOTDIR:-$HOME}/.zprezto/runcoms/zshrc"

# reset prompt every second (update clock)
# TRAPALRM is run every $TMOUT seconds
TMOUT=1
TRAPALRM() {
    if [ "$WIDGET" != "complete-word" ]; then
        zle reset-prompt
    fi
}


# coreutils in osx (with dircolors)
coreutils_dir=/usr/local/opt/coreutils/libexec/gnubin
if [ -d ${coreutils_dir} ]; then
  export PATH="${coreutils_dir}:$PATH"
fi

## Solarized dircolors
unalias ls
alias ls="ls --color=auto -F"
if (( $+commands[dircolors] )); then
  eval $(dircolors ~/.dircolors)
  zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
else
  export LS_COLORS="Gxfxcxdxbxegedabagacad"
  export CLICOLOR=YES
fi

## home bin
if [ -d $HOME/bin ]; then
  export PATH=$HOME/bin:$PATH
fi

# add linuxbrew if there
case $(uname) in
  Linux)
    BREW_ROOT=$HOME/.linuxbrew
    if [ -d $BREW_ROOT ]; then
      export PATH=$BREW_ROOT/bin:$PATH
      export LD_LIBRARY_PATH=$BREW_ROOT/lib:$LD_LIBRARY_PATH
      export MANPATH=$BREW_ROOT/share/man:$MANPATH
    fi
    ;;
  Darwin)
    if [ -e /usr/local/bin/brew ]; then
      export PATH="/usr/local/bin:$PATH"
    fi
    ;;
esac

# add heroku if there
if [ -d /usr/local/heroku ]; then
  export PATH="/usr/local/heroku/bin:$PATH"
fi

# add rvm if there
if (( $+commands[rbenv] )); then
  eval "$(rbenv init - zsh)"
fi

#
# base16 colors (needs ruby to generate templates, if missing)
#
base16 apply


# load enhanced less colors
source $HOME/.zsh/less-colors.zsh

# load more bindings
source $HOME/.zsh/bindings.zsh

# load tmux customizers
source $HOME/.zsh/tmux.zsh

# load local customizers
if [ -d $HOME/.zshrc.local.d ]; then
  for file ($HOME/.zshrc.local.d/*) source $file
fi

# init homeshick
if [[ -f $HOME/.homesick/repos/homeshick/homeshick.sh ]]; then
  source $HOME/.homesick/repos/homeshick/homeshick.sh
  [[ -n "$TMUX" ]] && homeshick --quiet refresh
fi

# customize git (XXX i'm sure there's a better way...)
_git_log_oneline_format='%Cred%h%Creset%Cgreen%d%Creset %s %C(blue)<%an>%Creset %C(yellow)[%cr]%Creset'

# Disable ^S ^Q to stop start the output
_disable_flow_control() {
  stty start undef stop undef
}
autoload -U add_zsh_hook
add-zsh-hook preexec _disable_flow_control 2>/dev/null || { echo 'zshrc: failed loading flow control hook.' }

if [ -f ~/.fzf.zsh ]; then
  if (( $+commands[ag] )); then
    export FZF_DEFAULT_COMMAND='pt -l -g=.'
  fi
  export FZF_DEFAULT_OPTS='-x'
  source ~/.fzf.zsh

  fpt() {
    [ $# -eq 0 ] && return
    local out cols
    if out=$(pt --color --nogroup "$@" | fzf --ansi); then
      setopt shwordsplit
      cols=(${out//:/ })
      vim ${cols[1]} +"normal! ${cols[2]}zz"
    fi
  }
fi

